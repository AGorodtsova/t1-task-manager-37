package ru.t1.gorodtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull Status status
    );

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @NotNull Status status
    );

    @NotNull
    Project update(@NotNull Project project);

}
