package ru.t1.gorodtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull String email);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull Role role);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull String email, @NotNull Role role);

    User update(@NotNull User user);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

}
