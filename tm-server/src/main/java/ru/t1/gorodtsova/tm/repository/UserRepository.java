package ru.t1.gorodtsova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.IUserRepository;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final String table = "tm_user";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return table;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password"));
        user.setEmail(row.getString("email"));
        user.setLastName(row.getString("last_name"));
        user.setFirstName(row.getString("first_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setRole(Role.toRole(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, password, first_name, " +
                "last_name, middle_name, email, role, locked)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setString(3, model.getPasswordHash());
            statement.setString(4, model.getFirstName());
            statement.setString(5, model.getLastName());
            statement.setString(6, model.getMiddleName());
            statement.setString(7, model.getEmail());
            statement.setString(8, model.getRole().getDisplayName());
            statement.setBoolean(9, model.getLocked());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @SneakyThrows
    @Override
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET login = ?, password = ?, email = ?, last_name = ?, " +
                "first_name = ?, middle_name = ?, role = ?, locked = ? WHERE id = ?;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getRole().getDisplayName());
            statement.setBoolean(8, user.getLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String passwordHash) {
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String email
    ) {
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final Role role
    ) {
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String email,
            @NotNull final Role role
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE login = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE email = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
