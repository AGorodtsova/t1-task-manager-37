package ru.t1.gorodtsova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.repository.ISessionRepository;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    private final String table = "tm_session";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return table;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setDate(row.getTimestamp("created"));
        session.setRole(Role.toRole(row.getString("role")));
        session.setUserId(row.getString("user_id"));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, created, role, user_id) " +
                        "VALUES (?, ?, ?, ?)", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getDate().getTime()));
            statement.setString(3, model.getRole().getDisplayName());
            statement.setString(4, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final String userId, @NotNull final Session model) {
        model.setUserId(userId);
        return add(model);
    }

}
