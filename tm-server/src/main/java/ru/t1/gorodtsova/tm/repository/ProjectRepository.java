package ru.t1.gorodtsova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private final String table = "tm_project";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return table;
    }

    @NotNull
    @SneakyThrows
    protected Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("descrptn"));
        project.setUserId(row.getString("user_id"));
        project.setStatus(Status.toStatus(row.getString("status")));
        project.setCreated(row.getTimestamp("created"));
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) {
        Project project = new Project(name, status);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        Project project = new Project(name, status);
        project.setUserId(userId);
        project.setDescription(description);
        return add(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, name, descrptn, status, created, user_id) " +
                        "VALUES (?, ?, ?, ?, ?, ?);",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getStatus().toString());
            statement.setTimestamp(5, new Timestamp(project.getCreated().getTime()));
            statement.setString(6, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET name = ?, descrptn = ?, status = ? WHERE id = ? AND user_id = ?;",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.setString(5, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

}
