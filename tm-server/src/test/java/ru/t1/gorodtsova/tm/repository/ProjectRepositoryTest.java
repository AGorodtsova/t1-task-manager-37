package ru.t1.gorodtsova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.service.ConnectionService;
import ru.t1.gorodtsova.tm.service.PropertyService;

import java.sql.Connection;

import static ru.t1.gorodtsova.tm.constant.ProjectTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER1;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);

    @Before
    public void init() {
        projectRepository.removeAll();
    }

    @After
    @SneakyThrows
    public void dropConnection() {
        connection.rollback();
        connection.close();
    }

    @Test
    public void add() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.findAll().get(0));
        Assert.assertEquals(USER1.getId(), projectRepository.findAll().get(0).getUserId());
    }

    @Test
    public void addAll() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST, projectRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST, projectRepository.findAll(USER1.getId()));
        Assert.assertNotEquals(USER1_PROJECT_LIST, projectRepository.findAll(USER2.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertNotEquals(USER2_PROJECT1, projectRepository.findOneById(USER1.getId(), USER2_PROJECT1.getId()));
        Assert.assertNull(projectRepository.findOneById(USER1.getId(), USER2_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.removeOneById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(projectRepository.findAll().contains(USER1_PROJECT1));
        Assert.assertTrue(projectRepository.findAll().contains(USER2_PROJECT1));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectRepository.removeOne(USER1.getId(), USER1_PROJECT1));
        Assert.assertFalse(projectRepository.findAll().contains(USER1_PROJECT1));
        Assert.assertTrue(projectRepository.findAll().contains(USER2_PROJECT1));
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST, projectRepository.findAll());
        projectRepository.removeAll(USER2.getId());
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        projectRepository.removeAll(USER1.getId());
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER2_PROJECT1);
        projectRepository.removeAll(USER1.getId());
        Assert.assertFalse(projectRepository.findAll().isEmpty());
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        projectRepository.add(USER1_PROJECT1);
        Assert.assertTrue(projectRepository.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectRepository.existsById(USER2_PROJECT1.getId()));
    }

    @Test
    public void createProjectName() {
        @NotNull final Project project = projectRepository.create(USER1.getId(), "test project");
        Assert.assertEquals(project, projectRepository.findOneById(project.getId()));
        Assert.assertEquals("test project", project.getName());
        Assert.assertEquals(USER1.getId(), project.getUserId());
    }

    @Test
    public void createProjectNameDescription() {
        @NotNull final Project project = projectRepository.create(USER1.getId(), "test project", "test description");
        Assert.assertEquals(project, projectRepository.findOneById(project.getId()));
        Assert.assertEquals("test project", project.getName());
        Assert.assertEquals("test description", project.getDescription());
        Assert.assertEquals(USER1.getId(), project.getUserId());
    }

}
