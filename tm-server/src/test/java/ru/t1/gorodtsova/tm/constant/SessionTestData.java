package ru.t1.gorodtsova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.model.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static Session USER1_SESSION1 = new Session();

    @NotNull
    public final static Session USER1_SESSION2 = new Session();

    @NotNull
    public final static Session USER1_SESSION3 = new Session();

    @NotNull
    public final static Session USER2_SESSION1 = new Session();

    @NotNull
    public final static Session ADMIN1_SESSION1 = new Session();

    @NotNull
    public final static Session ADMIN1_SESSION2 = new Session();

    @NotNull
    public final static List<Session> USER1_SESSION_LIST = Arrays.asList(USER1_SESSION1, USER1_SESSION2, USER1_SESSION3);

    @NotNull
    public final static List<Session> USER2_SESSION_LIST = Collections.singletonList(USER2_SESSION1);

    @NotNull
    public final static List<Session> ADMIN1_SESSION_LIST = Arrays.asList(ADMIN1_SESSION1, ADMIN1_SESSION2);

    @NotNull
    public final static List<Session> SESSION_LIST = new ArrayList<>();

    static {
        USER1_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.USER1.getId()));
        USER2_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.USER2.getId()));
        ADMIN1_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.ADMIN1.getId()));

        SESSION_LIST.addAll(USER1_SESSION_LIST);
        SESSION_LIST.addAll(USER2_SESSION_LIST);
        SESSION_LIST.addAll(ADMIN1_SESSION_LIST);

        SESSION_LIST.forEach(session -> session.setId("t-0" + SESSION_LIST.indexOf(session)));
    }

}
